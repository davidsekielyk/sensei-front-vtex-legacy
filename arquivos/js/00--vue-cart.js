function formatPriceCart(price) {
  price = String(price)
  const len = price.length 
  const decimal = price.substr(-2)
  const entero = price.substr(0,len-2)
  price = parseFloat(entero+"."+decimal)

  const re = '\\d(?=(\\d{' + 3 + '})+' + '\\D' + ')'
  let num = price.toFixed(Math.max(0, ~~2));
  const final = "$"+num.replace('.', ',').replace(new RegExp(re, 'g'), '$&' + ('.' || ','));
  
  return final
}

const vItemCart = {
  props: {
    item: Object,
    index: Number,
  },
  template: `
    <div class="v-cart__item" :class="{ invalid : invalid }">
      <div class="v-cart__item--img">
        <img :src="bestQuality(item.imageUrl)" :alt="item.name" />
      </div>
      <div class="v-cart__item--data">
        <div v-if="invalid" class="v-cart__item--info">
          <svg width="40" height="30" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.557779 16.9891L8.98076 2.4243C9.44913 1.61443 10.288 1.13086 11.2248 1.13086C12.1616 1.13086 13.0006 1.61443 13.4689 2.4243L21.8919 16.989C22.3603 17.799 22.3603 18.766 21.8919 19.5759C21.4235 20.3859 20.5846 20.8694 19.6478 20.8694H2.8019C1.86513 20.8694 1.02623 20.3858 0.557779 19.576C0.0893707 18.766 0.0893707 17.799 0.557779 16.9891ZM1.67588 18.9315C1.91092 19.3379 2.33186 19.5805 2.8019 19.5805H19.6478C20.1179 19.5805 20.5388 19.3379 20.7738 18.9315C21.0088 18.5251 21.0088 18.0399 20.7738 17.6335L12.3508 3.06879C12.1158 2.66239 11.6948 2.41979 11.2248 2.41979C10.7548 2.41979 10.3338 2.66239 10.0988 3.06879L1.67584 17.6335C1.44088 18.0399 1.44088 18.5251 1.67588 18.9315Z" fill="#FBA710"/>
            <path d="M10.5796 7.56299H11.8706V14.0075H10.5796V7.56299Z" fill="#FBA710"/>
            <path d="M11.2247 15.2964C11.6993 15.2964 12.0854 15.6819 12.0854 16.1557C12.0854 16.6295 11.6993 17.015 11.2247 17.015C10.7502 17.015 10.364 16.6295 10.364 16.1557C10.364 15.6819 10.7501 15.2964 11.2247 15.2964Z" fill="#FBA710"/>
          </svg>
          <div>El producto ya no se encuentra disponible.</div>
        </div>
        <div v-else class="v-cart__item--data-content">
          <div class="v-cart__item--name">
            {{ item.name }}
          </div>
          <div class="v-cart__item--values">
            <div class="v-cart__item--qt">
              Cantidad: <span>{{ item.quantity }}</span>
            </div>
            <div class="v-cart__item--price">
              <div class="v-cart__item--list-price">
                {{ listPrice }}
              </div>
              <div class="v-cart__item--best-price">
                {{ bestPrice }}
              </div> 
            </div>
          </div>
        </div>
        <div class="v-cart__item--delete" @click="removeItem(index)">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.5293 3.32031V5H4.4707V3.32031H7.2168L8.0332 2.5H11.9668L12.7832 3.32031H15.5293ZM5.25 15.8203V5.82031H14.75V15.8203C14.75 16.263 14.5892 16.6536 14.2676 16.9922C13.946 17.3307 13.5749 17.5 13.1543 17.5H6.8457C6.42513 17.5 6.05404 17.3307 5.73242 16.9922C5.41081 16.6536 5.25 16.263 5.25 15.8203Z" fill="#272727"/>
          </svg>
        </div>
      </div>
    </div>
  `,
  computed: {
    bestPrice(){
      return formatPriceCart(this.item.price * this.item.quantity)  
    },
    listPrice() {
      if (this.item.price < this.item.listPrice){
        return formatPriceCart(this.item.listPrice * this.item.quantity)
      }else {
        return ''
      }
    },
    invalid() {
      return this.item.availability !== "available"
    },
  },
  methods: {
    removeItem(index) {
      this.$emit('listenLoading')
      console.log('index',index)
      vtexjs.checkout.removeItems([{
        index: index,
        quantity: 0
      }])
      .done( orderForm => {
        console.log(orderForm)
        this.$emit('listenRemove')
      });
    },
    bestQuality(src) {
      return src.replace('-55-55','-120-120')
    }
  }
}

const vMontosCart = {
  props: {
    totalizers: Array,
    value: Number,
  },
  template: `
    <div class="v-cart__summary-montos">
      <div class="v-cart__content-montos">
        <div v-if="montos.subtotal.length" class="v-cart__montos">
          <span>Subtotal:</span>
          <span>{{ montos.subtotal }}</span>
        </div>
        <div v-if="montos.descuento.length" class="v-cart__montos">
          <strong>Estas ahorrando:</strong>
          <strong>{{ montos.descuento }}</strong>
        </div>
        <div v-if="montos.envio.length" class="v-cart__montos">
          <span>Costo del envio:</span>
          <span>{{ montos.envio }}</span>
        </div>
      </div>
      <div class="v-cart__end">
        <div class="v-cart__end--total">
          <strong>Total: </strong>
          <strong>{{ total }}</strong>
        </div>
        <a href="/checkout" class="v-cart__end--finalizar-pedido">
          <strong>Finalizar pedido</strong>
        </a>
      </div>
    </div>
  `,
  computed: {
    montos() {

      let montos = {
        subtotal: '',
        envio: '',
        descuento: '',
      }

      this.totalizers.forEach( el => {
        switch (el.id) {
          case "Items":
            montos.subtotal = formatPriceCart(el.value)  
            break;
          case "Shipping":
            montos.envio = formatPriceCart(el.value)
            break;
          case "Discounts":
            montos.descuento = formatPriceCart(el.value)
            break;
          default:
            break;
        }
      })

      return montos
    },
    total() {
      return formatPriceCart(this.value)
    }
  }
}

const vCart = {
  components: {
    'v-item-cart': vItemCart,
    'v-montos-cart': vMontosCart
  },
  mounted(){
    this.getOrderForm()
  },
  template: `
    <div class="v-cart" :class="{ show: show }">
      <div class="v-cart__overlay" @click="toggleCart(false)"></div>
      <div class="v-cart__info">
        <div class="v-cart__header" @click="toggleCart(false)">
          <div class="v-cart__header--title">MI CARRITO</div>
          <div class="v-cart__header--close">X</div>
        </div>
        <div class="v-cart__body">
          <div v-if="!orderForm.items.length" 
            class="v-cart__empty">
            <img src="/arquivos/carrito-vacio.png" alt="carrito vacio" />
            <span>No hay productos en el carrito</span>
          </div>
          <div v-else 
            class="v-cart__summary">
            <div class="v-cart__items">
              <v-item-cart 
                v-for="(item, index) in orderForm.items" 
                :key="item.id"
                :item="item"
                :index="index"
                @listenLoading="startLoading"
                @listenRemove="refreshCart"
              />
            </div>
            <v-montos-cart :totalizers="orderForm.totalizers" :value="orderForm.value"/>
            <div class="v-loading" v-if="loading">
              <div class="v-loading__spinner"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  data() {
    return {
      orderForm: {
        items: [],
        totalizers: [],
        value: 0
      },
      show: false,
      loading: false,
    }
  },
  methods: {
    getOrderForm(){
      vtexjs.checkout.getOrderForm()
      .done(orderForm => {
        console.log(orderForm)
        this.orderForm = orderForm
        this.loading = false
      });
    },
    toggleCart(estado) {
      this.show = estado
    },
    refreshCart() {
      console.log('hay que hacer un refresh')
      this.getOrderForm()
    },
    startLoading() {
      this.loading = true
    }
  }
}



let vmCart;
if($('#cart-content').length){
  vmCart = new Vue({
    el: '#cart-content',
    components: {
      'v-cart': vCart
    }
  })

  $("body").on("click",".header__icon-shopcart--open",function(){
    vmCart.$refs.cart.toggleCart(true)
  })
}