(()=>{  
    sliderPrincipal();
    carruselItems();
    setCarruselProd();
    carruselSmall();

})();

function banInjection(contentEspecifico){
    const listaBan = $(contentEspecifico).find(".box-banner")
    for (let i = 0; i < listaBan.length; i++) {
        let $this = listaBan.eq(i)
        contentEspecifico.find(".swiper-wrapper").append(
            '<div class="swiper-slide">'+$this.html()+'</div>'  // Agregar cada ban al slide
        );
        // $this.remove();
    }
    }

function sliderPrincipal(){
    if($(".slide-banner").length){
        let cantSlide = $(".slide-banner").length
        for (let i = 0; i < cantSlide; i++) {
            const contentEspecifico = $(".slide-banner").eq(i);
            banInjection(contentEspecifico)
        }
        // Como todo es sincronico se puede ejecutar el swiper al finalizar todo
        const swiperSlide = new Swiper('.slide-banner .swiper-container',{
            loop: true,
            autoplay: {
              delay: 6000,
              disableOnInteraction: false,
          },
            pagination: {
                el: '.slide-banner  .swiper-pagination',
                clickable: true,
                dynamicBullets: false,
            },
        })
    }
    }

function carruselItems(){
        if($(".items-section").length){
            const swiperItems = new Swiper('.items', {
                slidesPerView: 'auto',
                autoplay: false,
                loop: false,
                spaceBetween: 10,
                navigation: {
                  nextEl: null,
                  prevEl: null,
                },
                breakpoints: {
                  800: {
                    slidesPerView: 'auto',
                    spaceBetween: 10,
                    autoplay: false,
                    navigation: {
                        nextEl: null,
                        prevEl: null,
                      }
                  },
              
                }
              });
      
        }
        
        
    }

function setCarruselProd(){
        const carruselProd = $(".carrusel-prod")
        if(carruselProd.length){
            const cantCarruseles = carruselProd.length;
            for (let i = 0; i < cantCarruseles; i++) {
                const carrusel = carruselProd.eq(i)
                carruselInjection(carrusel);
                carruselType(carrusel);
                carruselImg(carrusel);
            }
        }
    }
        
function carruselInjection(carrusel){
        const listaProduct = carrusel.find(".prateleira .vitrina");
        
        for (let i = 0; i < listaProduct.length; i++) {
            let $this = listaProduct.eq(i).parent();
            carrusel.find(".swiper-wrapper").append(
                '<div class="swiper-slide">'+$this.html()+'</div>'  // Agregar cada producto al carrusel
            );
        }
        
    }
        
function carruselType(carrusel){
        
            if(carrusel.hasClass("type-estandar")){
                new Swiper('.carrusel-prod.type-estandar .swiper-container',{
                    spaceBetween: 20,
                    slidesPerView: 1.5,
                    loop: false,
                    freeMode: true,
                    navigation: {
                        nextEl: '.carrusel-prod .swiper-button-next',
                        prevEl: '.carrusel-prod .swiper-button-prev',
                    },
                    // autoplay: {
                    //     delay: 3200,
                    //     disableOnInteraction: false,
                    // },
                    breakpoints: {
                        600: {
                            slidesPerView: 3,

                        },
                        800: {
                            slidesPerView: 4,
                            navigation: {
                                nextEl: '.carrusel-prod .swiper-button-next',
                                prevEl: '.carrusel-prod .swiper-button-prev',
                            },
                        },
                    }
                });
            }
            
    }
        
function carruselImg(carrusel){
        console.log(carrusel)
        console.log(carrusel.find(".box-banner"))
        const boxBanner = carrusel.find(".box-banner")
        console.log(boxBanner)
        carrusel.find(".redirect").html(boxBanner)
    }

function carruselSmall(){
        if($(".slide-banner-small").length){
            let cantSlide = $(".slide-banner-small").length
            for (let i = 0; i < cantSlide; i++) {
                const contentEspecifico = $(".slide-banner-small").eq(i);
                banInjection(contentEspecifico)
            }
            // Como todo es sincronico se puede ejecutar el swiper al finalizar todo
            const swiperCategorias = new Swiper('.slide-banner-small .swiper-container',{
              slidesPerView: 'auto',
              spaceBetween: 20,
              navigation: {
                nextEl: null,
                prevEl: null,
              },
              breakpoints: {
               800: {
                  slidesPerView: 'auto',
                  spaceBetween: 20,
                  autoplay: false,
                  navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                  }
                },
            
              }
            })
        }
    }
