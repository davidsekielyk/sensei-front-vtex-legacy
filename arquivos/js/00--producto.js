$('body').on("click", "#btnFreteSimulacao", function(){
  console.log("se calculara el costo de envio"); 
  setTimeout(function(){ 
     $('.freight-values').html(function(){ 
        return $(this).html().replace(/úteis/g,' hábiles').replace(/útil/g,'hábil').replace(/Frete/g,'Envío').replace(/Grátis/g,'gratis').replace(/em/g,'en').replace(/dias/g,'días').replace(/Frete/g,'Envío').replace(/Grátis/g,'gratis').replace(/em/g,'en').replace(/CEP/g,'CP').replace(/ o /g,' el '); 
        }); 
        }, 2500);

});


window.onload = function(){
  $(".flag.envio-gratis").html("ENVÍO GRATIS");


};

(()=>{
    const IDsku = skuJson_0.productId.toString();
    imgCarrusel()
    redondeoDescuento()
    $("body").on("click",".btn-agregar-al-carrito",function(){
      sendSkuCheckout(IDsku , 1)
      .then(
        response => {
        console.log(response)
        if(response === "sin-stock"){
          sinStock()
        }else{
        stopLoading()


        }
  
  
      })
      .catch( err => {
        console.error(
          'error',
          err
        )
        loadingFailed()
      })

    })
   
    makeRequest()
    .then( data =>{
        dataProduct = data
        setInfo()
        financiacion()
        paymentMethods()
        paymentMethodsDesk()
        displayInstallmentsDesk()
    })
    setAccordion()
    setCarruselProd()
    if($(".productDescription").html() === ""){
      $(".product__description").css("display", "none")
    }else{
      $(".productDescription").addClass("descripcion")

    }
    $("#fade").modal({
      fadeDuration: 100
    });
    setTabs()
    
  })()



  
function imgCarrusel() {
    const listImg = $(".product__principal .thumbs a");
    for (let i = 0; i < listImg.length; i++) {
      const src = listImg.eq(i).attr("rel")
      const zoom = listImg.eq(i).attr("zoom")
      $(".product__thumbs .swiper-wrapper").append(`
        <div class="swiper-slide">
          <img src="${src}"/>
        </div>
      `);
      $(".product__gallery .swiper-wrapper").append(`
        <div class="swiper-slide">
          <img src="${zoom}" data-magnify-src="${zoom}"/>
        </div>
      `);


    }
  if(listImg.length === 1){
    $(".swiper-container.product__gallery").append('<div class="cant-fotos"><strong>'+listImg.length+' FOTO</strong></div>')


  }else{
    $(".swiper-container.product__gallery").append('<div class="cant-fotos"><strong>'+listImg.length+' FOTOS</strong></div>')

  }

  
    const galleryThumbs = new Swiper('.product__thumbs', {
      spaceBetween: 10,
      slidesPerView: 4,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      breakpoints:{
        1024: {
          direction: 'vertical',
          slidesPerView: 4,
          freeMode: false,
        }
      }
    });
    new Swiper('.product__gallery', {
      spaceBetween: 10,
      navigation: {
        nextEl: '.product__gallery .swiper-button-next',
        prevEl: '.product__gallery .swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs,
      },
      loop: true,
    });
  
    if ($(window).width() >= 768){
      $(".product__gallery img").magnify();
    }
  
  }

function sendSkuCheckout(idSku,qt){
  loading()
    return new Promise((res,rej)=>{
      const items = vtexjs.checkout.orderForm.items;

      const disponibles = skuJson_0.skus[0].availablequantity;


      for (let i = 0; i < items.length; i++) {
          const item = items[i];
          if(idSku == item.id){
              qt += item.quantity;    
          }
      }
      if( disponibles < qt){
        res("sin-stock")

      }



        const url = `/checkout/cart/add?qty=${qt}&seller=1&sku=${idSku}&sc=1&redirect=false`
        $.ajax({
            url: url,
            success: function(){
                res("ok")
            },
            error: function(error){
                console.error(error)
                rej("error")
            }
        })
    })

    vmCart.$refs.cart.getOrderForm();
    setQtCart();
}
function setQtCart(){

	let cantSkus = 0
	orderFormInnew.items.forEach(item => {
		cantSkus += item.quantity
	});

	console.log('cantidad total:',cantSkus)
	$(".shopcar__cant").html(cantSkus)

}

function sinStock(){
  const loader = document.querySelector(".loader");
  const btn = document.querySelector(".btn-agregar-al-carrito");
  const texto = document.querySelector(".texto-boton");
  const notification = document.querySelector(".product__cart-notification");
 

  texto.classList.add('hidden');
  loader.classList.add('loader-activo');
  btn.classList.add('inactivo');
  notification.classList.add('sin-stock');

  


  setTimeout(() => {
    notification.classList.remove('sin-stock');
    btn.classList.remove('inactivo');
    loader.classList.remove('loader-activo');
    texto.classList.remove('hidden');
    
  }, 4000);

}


function makeRequest(){

  return new Promise((res,rej) => {

      let productId = skuJson_0.productId
      let url = "/api/catalog_system/pub/products/search?fq=productId:"+productId 
      
      $.ajax({
          url: url,
          success: function(data){
              res(data)
          },
          error: function(error){
              console.error(error)
              rej(err)
          }
      })

  })

}

function setInfo(){


  // Porcentaje de descuento
  if($(".skuListPrice").length){
      const skuListPrice = parseFloat($(".skuListPrice").html().slice(2).replace(".","").replace(",","."));
      const skuBestPrice = parseFloat($(".skuBestPrice").html().slice(2).replace(".","").replace(",","."));
      const porcentaje = Math.round((skuListPrice - skuBestPrice)/(skuListPrice*0.01));
      $(".economia-de").append('<em class="valor-desc"><strong class="skuPerc">'+porcentaje+'% OFF</strong></em>');
  }



}

function setAccordion(){
  $(".acordeon").on("click", ".acordeon-cabecera", function() {
    $(this).toggleClass("active").next().slideToggle();
  });
}

function cambiarEmail(){
  let arr = $(".email-resenha");
  arr = Array.from(arr);
  arr.forEach( email => {
    if(email.innerHTML === 'endereço de e-mail não publicado'){
      email.innerHTML = 'Anónimo';
    }
  })
}

function setCarruselProd(){
  const carruselProd = $(".carrusel-prod")
  if(carruselProd.length){
      const cantCarruseles = carruselProd.length;
      for (let i = 0; i < cantCarruseles; i++) {
          const carrusel = carruselProd.eq(i)
          carruselInjection(carrusel);
          carruselType(carrusel);
          carruselImg(carrusel);
      }
  }
}
  
function carruselInjection(carrusel){
  const listaProduct = carrusel.find(".prateleira .vitrina");
  
  for (let i = 0; i < listaProduct.length; i++) {
      let $this = listaProduct.eq(i).parent();
      carrusel.find(".swiper-wrapper").append(
          '<div class="swiper-slide">'+$this.html()+'</div>'  // Agregar cada producto al carrusel
      );
  }
  
}
  
function carruselType(carrusel){
  
      if(carrusel.hasClass("type-estandar")){
          new Swiper('.carrusel-prod.type-estandar .swiper-container',{
              spaceBetween: 20,
              slidesPerView: 2,
              loop: false,
              freeMode: true,
              navigation: {
                  nextEl: '.carrusel-prod .swiper-button-next',
                  prevEl: '.carrusel-prod .swiper-button-prev',
              },
              // autoplay: {
              //     delay: 3200,
              //     disableOnInteraction: false,
              // },
              breakpoints: {
                  600: {
                      slidesPerView: 3,

                  },
                  800: {
                      slidesPerView: 5,
                      navigation: {
                          nextEl: '.carrusel-prod .swiper-button-next',
                          prevEl: '.carrusel-prod .swiper-button-prev',
                      },
                  },
              }
          });
      }
      
}
  
function carruselImg(carrusel){
  console.log(carrusel)
  console.log(carrusel.find(".box-banner"))
  const boxBanner = carrusel.find(".box-banner")
  console.log(boxBanner)
  carrusel.find(".redirect").html(boxBanner)
}



function formatPrice(price){
  price = String(parseFloat(price).toFixed(2))
  const len = price.length 
  const decimal = price.substr(-2)
  const entero = price.substr(0,len-2)
  price = parseFloat(entero+"."+decimal)

  const re = '\\d(?=(\\d{' + 3 + '})+' + '\\D' + ')'
  let num = price.toFixed(Math.max(0, ~~2));
  const final = "$"+num.replace('.', ',').replace(new RegExp(re, 'g'), '$&' + ('.' || ','));
  
  return final
}

function redondeoDescuento(){
  
    let descuento = $(".economia");
    descuento = Array.from(descuento);
    descuento.forEach( desc => {
      const texto = desc.innerHTML;
      const textoFinal = texto.slice(0, -3);
      desc.innerHTML = textoFinal

    });


}

function loading(){
  const loader = document.querySelector(".loader");
  const btn = document.querySelector(".btn-agregar-al-carrito");
  const texto = document.querySelector(".texto-boton");
  texto.classList.toggle('hidden');
  loader.classList.toggle('loader-activo');
  btn.classList.toggle('inactivo');


}
function stopLoading(){
  const loader = document.querySelector(".loader");
  const btn = document.querySelector(".btn-agregar-al-carrito");
  const texto = document.querySelector(".texto-boton");
  const btnCompleto = document.querySelector(".agregar-al-carrito");
  const notification = document.querySelector(".product__cart-notification");
 

  texto.classList.add('hidden');
  loader.classList.toggle('loader-activo');
  btn.classList.add('inactivo');
  btnCompleto.classList.add('exito');
  notification.classList.add('exito');

  


  setTimeout(() => {
    btnCompleto.classList.remove('exito');
    notification.classList.remove('exito');
    btn.classList.remove('inactivo');
    loader.classList.remove('loader-activo');
    texto.classList.remove('hidden');
    
  }, 4000);


   
 
}
function loadingFailed(){

  const loader = document.querySelector(".loader");
  const btn = document.querySelector(".btn-agregar-al-carrito");
  const texto = document.querySelector(".texto-boton");
  const btnCompleto = document.querySelector(".agregar-al-carrito");
  const notification = document.querySelector(".product__cart-notification");


  texto.classList.add('hidden');
  loader.classList.add('loader-activo');
  btn.classList.add('inactivo');
  btnCompleto.classList.add('error');
  notification.classList.add('error');

  


  setTimeout(() => {
    btnCompleto.classList.remove('error');
    notification.classList.remove('error');
    btn.classList.remove('inactivo');
    loader.classList.remove('loader-activo');
    texto.classList.remove('hidden');
    
  }, 6000);

}

function setTabs(){
  const labels = document.querySelectorAll(".accordion-item__label");
const tabs = document.querySelectorAll(".accordion-tab");

function toggleShow() {
	const target = this;
	const item = target.classList.contains("accordion-tab")
		? target
		: target.parentElement;
	const group = item.dataset.actabGroup;
	const id = item.dataset.actabId;

	tabs.forEach(function(tab) {
		if (tab.dataset.actabGroup === group) {
			if (tab.dataset.actabId === id) {
				tab.classList.add("accordion-active");
			} else {
				tab.classList.remove("accordion-active");
			}
		}
	});

	labels.forEach(function(label) {
		const tabItem = label.parentElement;

		if (tabItem.dataset.actabGroup === group) {
			if (tabItem.dataset.actabId === id) {
				tabItem.classList.add("accordion-active");
			} else {
				tabItem.classList.remove("accordion-active");
			}
		}
	});
}

labels.forEach(function(label) {
	label.addEventListener("click", toggleShow);
});

tabs.forEach(function(tab) {
	tab.addEventListener("click", toggleShow);
});


}

function financiacion() {
  
  // Lista de tarjetas que se quieren mostrar
  const whiteList = [
    'visa',
    'mastercard'
  ]

  // Get todas las formas de pago
  const commertialOffer = dataProduct[0].items[0].sellers[0].commertialOffer
  const installments = commertialOffer.Installments
  // console.log(
  //   'installments',
  //   installments
  // )

  // Filtrar solo las que no tienen interes y son mayores a 1 cuota.


  const listSinInteres = installments.filter(installment => installment.InterestRate === 0 && installment.NumberOfInstallments > 1)
  console.log(
    'listSinInteres',
    listSinInteres
  )
  // console.log(listSinInteres.length)

  if(listSinInteres.length != 0){

    // Ordenar la lista
    listSinInteres.sort((a, b) => {
      return b.NumberOfInstallments - a.NumberOfInstallments
    })
    console.log(
      'listSinInteres',
      listSinInteres
    )

    // Obtener solo las mejores cuotas de la misma tarjeta
    const listUniquePaymentSinInteres = Array.from(
      new Set(listSinInteres.map(a => a.PaymentSystemName))
    ).map(PaymentSystemName => {
      return listSinInteres.find(a => a.PaymentSystemName === PaymentSystemName)
    })
    console.log(
      'listUniquePaymentSinInteres',
      listUniquePaymentSinInteres
    )

    // Add HTML 
    listUniquePaymentSinInteres.forEach( item => {
  
      const systemName = item.PaymentSystemName.replace(/ /g,'-').toLowerCase()
      const isWhiteList = whiteList.find(target => target === systemName)
  
      if(isWhiteList !== undefined) {
        
        $(".product__financing--bests").append(`
          <div class="product__financing--item">
            <img src="/arquivos/logo-${systemName}.png" alt="logo tarjeta" class="product__financing--img" />
            <div class="product__financing--quota-value">
              <span class="product__financing--quota">
                ${item.NumberOfInstallments} CUOTAS SIN INTERÉS
              </span>
              <span class="product__financing--value">
                DE ${formatPrice(item.Value)}
              </span>
            </div>
          </div>
        `)

        // Mostrar el cuadro de financiacion
        $(".product__financing").removeClass("hidden")

      }
  
    })

  }
  else{
    const listConInteres = installments.filter(installment =>  installment.NumberOfInstallments > 1)
    console.log(
      'listConInteres',
      listConInteres
    )
    // console.log(listSinInteres.length)
  
    if(listSinInteres.length === 0){
  
      // Ordenar la lista
      listConInteres.sort((a, b) => {
        return b.NumberOfInstallments - a.NumberOfInstallments
      })
      console.log(
        'listConInteres',
        listConInteres
      )
  
      // Obtener solo las mejores cuotas de la misma tarjeta
      const listUniquePayment = Array.from(
        new Set(listConInteres.map(a => a.PaymentSystemName))
      ).map(PaymentSystemName => {
        return listConInteres.find(a => a.PaymentSystemName === PaymentSystemName)
      })
      console.log(
        'listUniquePayment',
        listUniquePayment
      )
  
      // Add HTML 
      listUniquePayment.forEach( item => {
    
        const systemName = item.PaymentSystemName.replace(/ /g,'-').toLowerCase()
        const isWhiteList = whiteList.find(target => target === systemName)
    
        if(isWhiteList !== undefined) {
          
          $(".product__financing--bests").append(`
            <div class="product__financing--item">
              <img src="/arquivos/logo-${systemName}.png" alt="logo tarjeta" class="product__financing--img" />
              <div class="product__financing--quota-value">
                <span class="product__financing--quota">
                  ${item.NumberOfInstallments} CUOTAS 
                </span>
                <span class="product__financing--value">
                  DE $${item.Value.toFixed(2)}
                  
                </span>
              </div>
            </div>
          `)
  
          // Mostrar el cuadro de financiacion
          $(".product__financing").removeClass("hidden")
          $(".product__financing--bests").addClass("interes")
  
        }
    
      })
  
    }

  }
 
  if($(".product__financing--item").length === 1){
    $(".product__financing--item").css("width", "100%");
    $(".product__financing--item").css("padding", "0.5rem 4rem");
    $(".product__financing--img").css("width", "15%");
  }


}


function paymentMethods(){



  const mediosDePago = {
    debitCardPaymentGroup: "Tarjetas de débito",
    creditCardPaymentGroup: "Tarjetas de crédito"
    

  }
  // Get todas las formas de pago
  const commertialOffer = dataProduct[0].items[0].sellers[0].commertialOffer
  const installments = commertialOffer.Installments

  let precioTotal = installments.find(installment => installment.InterestRate === 0 && installment.NumberOfInstallments === 1)

  precioTotal = precioTotal.Value;
  precioTotal = parseFloat(precioTotal).toFixed(2);


  //Efectivo (Información estática salvo precio)
  $(".product__financing-container .acordeon__container .acordeon").append(
    `<div class="acordeon-cabecera efectivo">
      <span>
      <p>En Efectivo</p><span class="monto">$ ${precioTotal}</span>
      </span>
      <svg width="8" height="12" viewBox="0 0 8 12"fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.15234 0.5L7.65234 6L2.15234 11.5L0.863281 10.2109L5.07422 6L0.863281 1.78906L2.15234 0.5Z"fill="#28292B" />
      </svg>
    </div>
    <div class="acordeon-contenido efectivo">
      <span>
        <img src="/arquivos/logo-pagofacil.png" alt="Pago Fácil" />
        <img src="/arquivos/logo-rapipago.png" alt="Rapipago" />
      </span>
    </div>
    `)
 
  const arrayMediosDePagoRepetidos = [];

  installments.forEach( installment => {
    arrayMediosDePagoRepetidos.push(installment.PaymentSystemGroupName)
  })

  const arrayMediosDePago = Array.from(new Set(arrayMediosDePagoRepetidos));

  arrayMediosDePago.reverse()

  // Display Medios de Pago Dinámicos
  arrayMediosDePago.map( medio => {
   
    if(mediosDePago[medio]){
    
      $(".product__financing-container .acordeon__container .acordeon").append(
        `<div class="acordeon-cabecera ${medio}">
          <span>
          <p>${mediosDePago[medio]}</p><span class="monto">$ ${precioTotal}</span>
          </span>
          <svg width="8" height="12" viewBox="0 0 8 12"fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.15234 0.5L7.65234 6L2.15234 11.5L0.863281 10.2109L5.07422 6L0.863281 1.78906L2.15234 0.5Z"fill="#28292B" />
          </svg>
        </div>
        <div class="acordeon-contenido ${medio}"><span></span></div>
        `)
      
    }
  })

  //Tarjetas de Débito

  const tarjetasDeDebitoRepetidas = []

  installments.forEach(installment => {
    if(installment.PaymentSystemGroupName === 'debitCardPaymentGroup' ){
     tarjetasDeDebitoRepetidas.push(installment.PaymentSystemName)
    }
  })

  const tarjetasDeDebito = Array.from(new Set(tarjetasDeDebitoRepetidas))


  tarjetasDeDebito.map( tarjeta => {
    const textoTarjeta = tarjeta.split(" ").join("").toLowerCase();
    $(".acordeon-contenido.debitCardPaymentGroup span").append(`
  
      <img class="logo-tarjeta" src="/arquivos/logo-${textoTarjeta}.png" alt="${tarjeta}" />
    
      `)
  })
  
  //Tarjetas de Crédito

  const tarjetasDeCreditoRepetidas = []

  installments.forEach(installment => {
    if(installment.PaymentSystemGroupName === 'creditCardPaymentGroup' ){
      tarjetasDeCreditoRepetidas.push(installment.PaymentSystemName)
    }
  })

  const tarjetasDeCredito = Array.from(new Set(tarjetasDeCreditoRepetidas))

  tarjetasDeCredito.map( tarjeta => {
    const textoTarjeta = tarjeta.split(" ").join("").toLowerCase();

    $(".acordeon-contenido.creditCardPaymentGroup").append(`
      <div class="acordeon-cabecera ${tarjeta}">
      <img class="logo-tarjeta" src="/arquivos/logo-${textoTarjeta}.png" alt="${tarjeta}"/><span class="bandera ${tarjeta} highlight"></span>
      <svg width="8" height="12" viewBox="0 0 8 12"fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2.15234 0.5L7.65234 6L2.15234 11.5L0.863281 10.2109L5.07422 6L0.863281 1.78906L2.15234 0.5Z"fill="#28292B" />
      </svg>
      </div>
      <div class="acordeon-contenido ${tarjeta}"></div>
    `)
    
  })

  displayInstallments(tarjetasDeCredito, installments);
  showBanderas(tarjetasDeCredito, installments)

 

}

function displayInstallments(tarjetasDeCredito,installments){

  $(".creditCardPaymentGroup .acordeon-contenido").prepend(`
  <p class="table-header"><span>Nº de cuotas</span><span>Valor</span></p>
  `)
  installments.forEach(installment => {
  if(tarjetasDeCredito.includes(installment.PaymentSystemName)){
    let nombreClase = installment.PaymentSystemName;
    
    if(nombreClase==="American Express"){
      nombreClase= "American"
    }
    if(installment.NumberOfInstallments === 1){
      $(`.acordeon-contenido.${nombreClase}`).append(`
      <p><span class="cuota-mobile"><span>${installment.NumberOfInstallments} cuota  ${installment.InterestRate === 0 ? `<span class="highlight"> sin interés</span>` : ''}</span><span> <span class="valor-cuota">  $${installment.Value.toFixed(2)}</span> </p>

     
      `)

    }else{
      $(`.acordeon-contenido.${nombreClase}`).append(`
      <p><span class="cuota-mobile"><span>${installment.NumberOfInstallments} cuotas  ${installment.InterestRate === 0 ? `<span class="highlight"> sin interés</span>` : ''}</span><span> <span class="valor-cuota">  $${installment.Value.toFixed(2)}</span> </p>
    
      `)
     

    } 
  }
  
  })


}

function showBanderas(tarjetasDeCredito, installments){
  let cuotasSinInteres = installments.filter(i => i.InterestRate === 0 );

 
  tarjetasDeCredito.map( tarjeta => {
    cuotasSinInteres.forEach(cuota => {
      if(tarjetasDeCredito.includes(cuota.PaymentSystemName)){
        let nombreClase = cuota.PaymentSystemName;
        
        if(nombreClase==="American Express"){
          nombreClase= "American"
        }
      let cuotasArr= [];
      if(cuota.PaymentSystemName === tarjeta && cuota.NumberOfInstallments > 1){
         cuotasArr = [...cuotasArr, cuota.NumberOfInstallments];
         $(`.bandera.${nombreClase}`).html('')
         $(`.bandera.${nombreClase}`).append(`
         ${cuotasArr} cuotas sin interés
         `)
      }}
    })
     
  })

}

function paymentMethodsDesk(){



  const mediosDePago = {
    debitCardPaymentGroup: "Tarjetas de débito",
    creditCardPaymentGroup: "Tarjetas de crédito"
    

  }
  // Get todas las formas de pago
  const commertialOffer = dataProduct[0].items[0].sellers[0].commertialOffer
  const installments = commertialOffer.Installments

  let precioTotal = installments.find(installment => installment.InterestRate === 0 && installment.NumberOfInstallments === 1)

  precioTotal = precioTotal.Value;
  precioTotal = parseFloat(precioTotal).toFixed(2);


  //Efectivo (Información estática salvo precio)
  $(".product__financing-container-desk").append(
    `<h3>Tarjetas y Promociones</h3>
      <div class="efectivo">
        <p>Efectivo</p><span class="monto">$ ${precioTotal}</span>
        <div class="contenido efectivo">
          <span class="payment__logo">
            <img src="/arquivos/logo-pagofacil.png" alt="Pago Fácil" />
          </span>
          <span class="payment__logo">
            <img src="/arquivos/logo-rapipago.png" alt="Rapipago" />
          </span>
        </div>
      </div>
    
    `)
 
  const arrayMediosDePagoRepetidos = [];

  installments.forEach( installment => {
    arrayMediosDePagoRepetidos.push(installment.PaymentSystemGroupName)
  })

  const arrayMediosDePago = Array.from(new Set(arrayMediosDePagoRepetidos));

  arrayMediosDePago.reverse()

  // Display Medios de Pago Dinámicos
  arrayMediosDePago.map( medio => {
   
    if(mediosDePago[medio]){
    
      $(".product__financing-container-desk").append(
        `<div class="contenedor ${medio}">
          <p>${mediosDePago[medio]}</p><span class="monto">$ ${precioTotal}</span>
          <div class="contenido ${medio}"></div>
        </div>
        
        `)
      
    }
  })

  //Tarjetas de Débito

  const tarjetasDeDebitoRepetidas = []

  installments.forEach(installment => {
    if(installment.PaymentSystemGroupName === 'debitCardPaymentGroup' ){
     tarjetasDeDebitoRepetidas.push(installment.PaymentSystemName)
    }
  })

  const tarjetasDeDebito = Array.from(new Set(tarjetasDeDebitoRepetidas))


  tarjetasDeDebito.map( tarjeta => {
    const textoTarjeta = tarjeta.split(" ").join("").toLowerCase();
    $(".contenido.debitCardPaymentGroup").append(`
    <span class="payment__logo">
      <img class="logo-tarjeta" src="/arquivos/logo-${textoTarjeta}.png" alt="${tarjeta}" />
    </span>
      `)
  })
  
  //Tarjetas de Crédito

  const tarjetasDeCreditoRepetidas = []

  installments.forEach(installment => {
    if(installment.PaymentSystemGroupName === 'creditCardPaymentGroup' ){
      tarjetasDeCreditoRepetidas.push(installment.PaymentSystemName)
    }
  })

  const tarjetasDeCredito = Array.from(new Set(tarjetasDeCreditoRepetidas))

  tarjetasDeCredito.map( tarjeta => {
    const textoTarjeta = tarjeta.split(" ").join("").toLowerCase();

    $(".contenido.creditCardPaymentGroup").append(`
      <div class="contenedor ${tarjeta} payment__logo">
      <label>
      <input type="radio" name="credit-card" class="credit-card" value=${tarjeta} onchange="displayInstallmentsDesk()" ${tarjeta === "Visa" && "checked" } />
      <img class="logo-tarjeta" src="/arquivos/logo-${textoTarjeta}.png" alt="${tarjeta}"/><span class="bandera ${tarjeta}"></span>
      </label>
      </div>
      
      
    `)
   
   
    
  })

  $(".contenedor.creditCardPaymentGroup").append(`
  <div class="installments__container"></div>
 `)

  displayInstallmentsDesk();
  showBanderasDesk(tarjetasDeCredito, installments);



}

function displayInstallmentsDesk(){
   // Get todas las formas de pago
   const commertialOffer = dataProduct[0].items[0].sellers[0].commertialOffer;
   const installments = commertialOffer.Installments;

   let opcionActiva = $('input[name=credit-card]:checked').val();
   if(opcionActiva === "American Express"){
     opcionActiva = "American" 
   }
   $(`.creditCardPaymentGroup .contenedor`).removeClass("active");
   $(`.creditCardPaymentGroup .contenedor.${opcionActiva}`).addClass("active")

   $(".installments__container").html('');

   $(".creditCardPaymentGroup .installments__container").prepend(`
   <p class="table-header"><span>Nº de cuotas</span><span>Valor total</span></p>
   `)

  const tarjetas =Array.from(document.querySelectorAll(".credit-card"));
  tarjetas.forEach( tarjeta => {
  if(tarjeta.checked){
    if(tarjeta.value ==="American"){
      tarjeta.value = "American Express"
    }

    installments.forEach( i => {
      if(i.PaymentSystemName === tarjeta.value){
        if(i.NumberOfInstallments === 1){
        
          $(".installments__container").append(`
          <div class="installment__info"><p>${i.PaymentSystemName} ${i.NumberOfInstallments} pago de $${i.Value.toFixed(2)} ${i.InterestRate === 0 ? `<span class="highlight">sin interés</span>`: ''}</p><span class="valor__total"> $${i.TotalValuePlusInterestRate.toFixed(2)}</span></div>
        `)

        }else{
          $(".installments__container").append(`
          <div class="installment__info"><p>${i.PaymentSystemName} ${i.NumberOfInstallments} pagos de $${i.Value.toFixed(2)} ${i.InterestRate === 0 ? `<span class="highlight"> sin interés</span>`: ''}</p><span class="valor__total"> $${i.TotalValuePlusInterestRate.toFixed(2)}</span></div>
        `)

        }

      }
    })
    
  }
  })


}
function showBanderasDesk(tarjetasDeCredito, installments){
  let cuotasSinInteres = installments.filter(i => i.InterestRate === 0 );

 
  tarjetasDeCredito.map( tarjeta => {
    cuotasSinInteres.forEach(cuota => {
      if(tarjetasDeCredito.includes(cuota.PaymentSystemName)){
        let nombreClase = cuota.PaymentSystemName;
        
        if(nombreClase==="American Express"){
          nombreClase= "American"
        }
      if(cuota.PaymentSystemName === tarjeta && cuota.NumberOfInstallments > 1 ){
        $(`.creditCardPaymentGroup .contenedor.${nombreClase} label .bandera__desk`).html('');
       $(`.creditCardPaymentGroup .contenedor.${nombreClase} label`).append(`
        <span class="bandera__desk">Sin interés</span>
       `)
      }}
    })
     
  })

}
