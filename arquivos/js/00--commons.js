console.log('funciona')

// ENVIAR DATOS SUSCRIPCIÓN NEWSLETTER A MASTERDATA
function sendData(input){

    let url = "/api/dataentities/NL/documents/";
    const data = {
        "email" : $(input).val()

    }

    $.ajax({
		headers: {
			"Accept": "application/vnd.vtex.ds.v10+json",
			"Content-Type": "application/json"
		},
		data: JSON.stringify(data),
		type: 'PATCH',
		url: url,
		success: function(data, textStatus, xhr){
            console.log(data);
            console.log(xhr.status)
		},
		error: function(e){
            console.error(e)
		}
    });

    $(input).val("");
    const mensaje = document.createElement('div');
    mensaje.classList.add('alert');
    mensaje.classList.add('alert-success');
    mensaje.classList.add('mensaje-nl');
    const texto = document.createTextNode('Se enviaron los datos correctamente. Pronto recibirás novedades.');
    mensaje.appendChild(texto);
    
    if(input === "#email"){
        setTimeout(function(){ 
        
            $(".newsletter-contenido").append(mensaje);
    
    
         }, 500);
    
         setTimeout(function(){ 
            
            mensaje.style.display= 'none';
    
    
         }, 5000);

    }
    else if(input === "#email-footer"){
        setTimeout(function(){ 
        
            $(".col.newsletter").append(mensaje);
    
    
         }, 500);
    
         setTimeout(function(){ 
            
            mensaje.style.display= 'none';
    
    
         }, 5000);

    }
  
}