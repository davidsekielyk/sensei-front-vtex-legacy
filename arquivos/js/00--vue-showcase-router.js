const routes = [
  {
    path: '*',
    component: Magia
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

const app = new Vue({
  router,
  store
}).$mount('#v-showcase');