const vTipoFiltro = {
  props: {
    nameTipoFiltro: String,
    listaTipoFiltro: Array,
    title: String
  },
  template: `
    <div class="v-filtros__tipo" :class="[nameTipoFiltro, title]">
      <div v-for="(tipoFiltro, index) in listaTipoFiltro"
        :key="index"
        :class="tipoFiltro.name"
        class="v-filtros__tipo--list">
        <details v-if="tipoFiltro.filtros.length" class="v-filtros__tipo--details">
          <summary class="v-filtros__title">
            {{ tipoFiltro.name }}
          </summary>
          <div class="v-filtros__content">
            <span v-for="(filtro, index) in tipoFiltro.filtros" 
              :key="index" 
              :data-fetch="filtro.dataFetch"
              :title="filtro.title"
              @click="aplicarFiltro(filtro)"
              class="v-filtro"
              :class="isVerTodos(filtro.name)">
              <span class="v-filtros__text">
                {{ filtro.name }}
              </span>
            </span>
          </div>
        </details>
      </div>
    </div>
  `,
  computed: {
    origin() {
      return window.location.origin
    },
    lid() {
      const url = new URL(window.location.href)
      const lid = url.searchParams.get("lid")
      return lid ? `&lid=${lid}` : ''
    },
  },
  methods: {
    aplicarFiltro(filtro) {
      this.$store.commit('SET_ACUM',filtro)
      this.changeURL(filtro.dataFetch)
    },
    changeURL(url) {
      const path = url.replace(this.origin,"")
      const newUrl = this.lid.length ? path+this.lid : path
      router.push(newUrl)
    },
    isVerTodos(name) {
      let data = ''
      let indexOfVerTodos = name.indexOf('Ver todo')
      if (indexOfVerTodos !== -1) {
        data += 'ver-todo '
      }
      if (this.nameTipoFiltro === 'categorias' && 
        this.listaTipoFiltro.length === 1 && 
        indexOfVerTodos !== -1) 
      {
        data += 'hidden'
      }
      return data
    },
  }
}

const vAcumFiltro = {
  template: `
    <div v-if="acum.length" 
      class="v-filtros__acum">
      <h3 class="v-filtros__acum--title">Filtros Aplicados</h3>
      <div class="v-filtros__acum--list">
        <span v-for="(filtro, index) in acum" 
          :key="index" 
          :title="filtro.title"
          :path="filtro.path"
          :param="filtro.param"
          @click="quitarFiltro(filtro)"
          class="v-filtro__acum">
          <span class="v-filtro__acum--text">
            {{ filtro.title }}
          </span>
          <span class="v-filtro__acum--close">
            X
          </span>
        </span>
      </div>
      <div class="v-filtros__acum--limpiar" @click="limpiarFiltros">
        <span>X LIMPIAR FILTROS</span>
      </div>
    </div>
  `,
  data() {
    return {
      url: encodeURI(decodeURI(this.$route.fullPath)),
      init: window.location.pathname + window.location.search
    }
  },
  computed: {
    ...Vuex.mapGetters([
      'findPage'
    ]),
    acum(){
      const page = this.findPage(this.url)
      if ( page === undefined ){
        return []
      }
      this.$store.commit('REPLACE_ALL_ACUM',[...page.acum])
      return page.acum
    }
  },
  methods: {
    quitarFiltro(filtro) {
      this.$store.commit('REMOVE_ACUM',filtro)
      this.changeURL(filtro.path,filtro.param)
    },
    changeURL(path,param) {
      if(path[0] === '/'){
        path = path.replace('/','')
      }
      const listPaths = path.split('/')

      const pathActual = window.location.pathname
      const listPathsActual = pathActual.split('/')

      listPaths.forEach( path => {
        const index = listPathsActual.indexOf(path)
        listPathsActual.splice(index,1)
      })
      
      let url = listPathsActual.join('/')
      if(url[0] !== '/'){
        url = '/'+url
      }
      console.log('url sin params',url)

      console.log("param",param)
      url += window.location.search
      if(param.length){
        url = url.replace(param,'')
      }
      console.log('url con params',url)

      router.push(url)
    },
    getFiltrosAcum(url) {
      const page = this.$store.getters.findPage(url)
      if ( page !== undefined ){
        this.testAcum = page.acum
      }
    },
    limpiarFiltros() {
      router.push(this.init)
    }
  },
  watch: {
    $route( to, from ) {
      console.log('cambio - hay que obtener los nuevos acum')
      console.log(to.fullPath)
      this.url = encodeURI(decodeURI(to.fullPath))
    }
  }
}

const vFiltros = {
  components: {
    'v-tipo-filtro': vTipoFiltro,
    'v-acum-filtro': vAcumFiltro
  },
  props: {
    filtros: String
  },
  template: `
    <aside class="v-filtros">
      <v-acum-filtro />
      <div class="v-filtros__list">
        <v-tipo-filtro
          v-for="(filtro, index) in tipoFiltro"
          :key="index"
          :title="filtro.title" 
          :nameTipoFiltro="filtro.nameTipoFiltro"
          :listaTipoFiltro="filtro.listaTipoFiltro"
        />
      </div>
    </aside>
  `,
  data() {
    return {
      page: document.querySelector('body').className,
      blackListAtributos: []
    }
  },
  computed: {
    marcas() {
      const filtros = $(this.filtros).find('ul.Marca').find('a') 
      return [{
        name: 'Marcas',
        filtros: Array.prototype.map.call(filtros, filtro => {
          return {
            name: $(filtro).html(),
            title: $(filtro).attr('title'),
            dataFetch: $(filtro).attr("href"),
            path: this.getPathFiltro($(filtro).attr("href")),
            param: ''
          }
        })
      }]
    },
    atributos() {
      let atributos = $(this.filtros).find('h5:not([class*="Marca"])')
      atributos = this.getWhiteListAtributos(atributos)
      return Array.prototype.map.call(atributos, atributo => {
        const filtros = $(atributo).next().find('a')
        return {
          name: $(atributo).text().toLowerCase(),
          filtros: Array.prototype.map.call(filtros, filtro => {
            const urlDataFetch = $(filtro).attr("href")
            const title = $(filtro).attr('title')
            return {
              name: $(filtro).html(),
              title: title,
              dataFetch: urlDataFetch,
              path: this.getPathFiltro(urlDataFetch),
              param: this.getParamFiltro(urlDataFetch,title)
            }
          })
        }
      })
    },
    categorias() {
      let categorias = $(this.filtros).find('h4')
      if( this.page.indexOf('categoria') === -1 && this.page.indexOf('departamento') === -1 ){
        categorias = $(this.filtros).find('h3')
      }

      return Array.prototype.map.call(categorias, categoria => {

        const subcategorias = $(categoria).next().find('a')
        const filtros = Array.prototype.map.call(subcategorias, subcategoria => {
          return {
            name: $(subcategoria).html(),
            title: $(subcategoria).attr('title'),
            dataFetch: $(subcategoria).attr("href"),
            path: this.getPathFiltro($(subcategoria).attr("href")),
            param: ''
          }
        })

        const filtroCateg = $(categoria).find('a')
        const nameCategoria = filtroCateg.text().toLowerCase()

        if (filtros.length === 0) {

          const dataFetch = filtroCateg.attr('href')
          const urlDataFetch = new URL(dataFetch)
          const pathDataFetch = urlDataFetch.pathname.toLowerCase()
          const pathActual = this.$route.path.toLowerCase()

          if (pathActual === pathDataFetch) {
            return {
              name: nameCategoria,
              filtros: filtros
            }
          }

        }

        return {
          name: nameCategoria,
          filtros: [
            ...filtros,
            {
              name: `Ver todo en ${nameCategoria}`,
              title: filtroCateg.attr('title'),
              dataFetch: filtroCateg.attr("href"),
              path: this.getPathFiltro(filtroCateg.attr("href")),
              param: ''
            }
          ]
        }

      })
    },
    tipoFiltro() {
      return [
        {
          nameTipoFiltro: 'categorias',
          listaTipoFiltro: this.categorias,
          title: this.getTitle('categorias'),
        },
        {
          nameTipoFiltro: 'marcas',
          listaTipoFiltro: this.marcas,
          title: this.getTitle('marcas'),
        },
        {
          nameTipoFiltro: 'atributos',
          listaTipoFiltro: this.atributos,
          title: this.getTitle('atributos'),
        },
      ]
    }
  },
  methods: {
    filtrar(url) {
      console.log('url que filtra',url)
      // UX
      this.$emit('listenLoading',true)
      initGoToTopPage = true
      goToTopPage()

      // Logica
      const page = this.$store.getters.findPage(url)
      // const acum = [...this.acum]
      if ( page === undefined ) {

        console.log('tiene que buscar')

        this.$store.dispatch('fetchPage', url)
        .then( newPage => {
          this.replaceContent(newPage)
        })

      } 
      else {

        console.log('ya tiene los datos')

        this.replaceContent(page)
      }
    },
    replaceContent(page) {
      this.$emit('listenProductFiltrados',page)
      this.$emit('listenLoading',false)
    },
    getDifference(a, b) {
      let i = 0;
      let j = 0;
      let result = "";
      
      while (j < b.length) {
        if (a[i] != b[j] || i == a.length)
          result += b[j];
        else
          i++;
        j++;
      }
      return result;
    },
    getPathFiltro(urlFiltro) {
      const url = new URL(urlFiltro)
      const pathActual = encodeURI(decodeURI(this.$route.path))
      return this.getDifference(pathActual, url.pathname)
    },
    getParamFiltro(urlDataFetch,title) {
      const url = new URL(urlDataFetch)
      const params = new URLSearchParams(url.search)
      const map = params.get('map')
      if(map === null){
        return ''
      }
      const listParams = map.split(',')
      const listPath = url.pathname.replace('/','').split('/')
      const index = listPath.indexOf(encodeURI(decodeURI(title)))
      if(index < 0) {
        return ''
      }
      return listParams[index]
    },
    getWhiteListAtributos(atributos) {
      return Array.prototype.filter.call(atributos, atributo => {
        const name = $(atributo).text()
        const status = this.blackListAtributos.find(attr => attr === name)
        if (status === undefined) {
          const contenFiltro = $(atributo).next()
          const filtroAplicado = $(contenFiltro).find('a.ver-filtros')
          if (!filtroAplicado.length) {
            return atributo
          }
        }
      })
    },
    getTitle(tipo) {
      if (tipo === 'categorias' &&
          this.categorias.length >= 1 && 
          this.categorias[0].filtros.length > 0) 
      {
        return 'mostrar-categorias'
      }

      else if (tipo === 'marcas' && 
              (this.marcas[0].filtros.length > 0  ||
              this.atributos.length > 0))
      {
        return 'mostrar-filtrar-por'
      }

      else {
        return ''
      }
    },
  },
  watch: {
    $route( to, from ) {
      console.log('to',to)
      this.filtrar(encodeURI(decodeURI(to.fullPath)))
    }
  }
};

const vPaginador = {
  mounted() {
    this.saveURLNextPage()
    this.calculateNextPage()
  },
  props: {
    filtrosAplicados: Boolean
  },
  template: `
    <div class="v-paginador">
      <div v-if="showBtn" 
        class="v-paginador__btn"
        @click="sendProducts">
        Mostrar más productos
      </div>
    </div>
  `,
  data() {
    return {
      pagina: 1,
      productos: undefined
    }
  },
  computed: {
    url() {
      const nextPage = this.pagina + 1
      const querys = this.$route.query
      const params = $.isEmptyObject(querys) ? `?PageNumber=${nextPage}` : `&PageNumber=${nextPage}`
      return window.location.origin + this.$route.fullPath + params
    },
    showBtn() {
      return this.productos !== undefined
    },
    fullPath() {
      return encodeURI(decodeURI(this.$route.fullPath))
    }
  },
  methods: {
    getProductNextPage() {
      return new Promise((res,rej) => {
        // console.log("url",this.url)
        $.ajax({
          url: this.url,
          success: page => {
            const productos = $(page).find(".vitrine .prateleira").html()
            res(productos)
          } 
        })
      }) 
    },
    calculateNextPage() {
      this.getProductNextPage()
      .then(productos => {
        this.productos = productos
      })
    },
    sendProducts() {
      this.$emit('listenMoreProducts',{
        productos: this.productos,
        url: this.fullPath
      })
      this.resetComponent(this.pagina)
    },
    resetComponent(pagina) {
      this.productos = undefined
      this.pagina = pagina + 1
      this.calculateNextPage()
    },
    saveURLNextPage() {
      let urlDestino = ''
      $("body").on("click", "a", function() {
        console.log($(this).attr('href'))
        urlDestino = $(this).attr('href')
      })
      
      window.addEventListener("beforeunload", event => {
        console.log(event)
        sessionStorage.setItem('urlOrigen',this.fullPath)
        sessionStorage.setItem('urlDestino',urlDestino)
        sessionStorage.setItem('storage',JSON.stringify(this.$store.state))
      });
    }
  },
  watch: {
    filtrosAplicados(val) {
      if(val) {
        this.resetComponent(0)
        this.$emit('listenFiltrosAplicados')
      }
    }
  }
}

const vOrdenar = {
  template: `
    <div class="v-ordenar">
      <ul class="v-ordenar__list">
        <li class="v-ordenar__title">
          <span>Ordenar por:</span>
        </li>
        <li v-for="(item, index) in items"
          :key="index"
          class="v-ordenar__item">
          <span v-if="item.active" 
            class="v-ordenar__item--active"
            @click="removeOrdenar(item.metodo)">
            {{ item.name }}
            <span class="v-ordenar__item--close">X</span>
          </span>
          <span v-else @click="aplicarOrdenar(item.metodo)">
            {{ item.name }}
          </span>
        </li>
      </ul>
    </div>
  `,
  data() {
    return {
      items: [
        {
          name: 'Menor Precio',
          metodo: 'OrderByPriceASC',
          active: false
        },
        {
          name: 'Mayor Precio',
          metodo: 'OrderByPriceDESC',
          active: false
        },
        {
          name: 'Mejor Descuento',
          metodo: 'OrderByBestDiscountDESC',
          active: false
        },
        {
          name: 'Lanzamientos',
          metodo: 'OrderByReleaseDateDESC',
          active: false
        },
      ]
    }
  },
  methods: {
    aplicarOrdenar(metodo) {
      this.changeURL(metodo)
      this.setActive(metodo)
    },
    changeURL(metodo) {
      const querys = this.$route.query
      let url = this.$route.fullPath

      if($.isEmptyObject(querys)) {
        url += `?O=${metodo}`
      }
      else if($.isEmptyObject(querys.O)) {
        url += `&O=${metodo}`
      }
      else {
        url = url.replace(querys.O, metodo)
      }

      router.push(url)
    },
    setActive(metodo) {
      this.items.forEach( (item, index) => {
        this.items[index].active = false
        if(item.metodo === metodo){
          this.items[index].active = true
        }
      })
    },
    removeOrdenar(metodo) {
      this.setActive('')
      let url = this.$route.fullPath
      let order = `O=${metodo}`
      url = url.replace(order,'')
      router.push(url)
    }
  }
}

const vProductos = {
  components: {
    'v-ordenar': vOrdenar,
    'v-paginador': vPaginador
  },
  props: {
    productos: String
  },
  template: `
    <article class="article">
      <v-ordenar />
      <div 
        v-html="dataProductos"
        :class="{efecto:efectos}" 
        class="vitrines"> 
      </div>
      <v-paginador 
        @listenMoreProducts="addMoreProducts($event)" 
        :filtrosAplicados="filtrosAplicados"
        @listenFiltrosAplicados="changeStateFiltrosAplicados" />
    </article>
  `,
  data() {
    return {
      dataProductos: this.productos,
      filtrosAplicados: false,
      efectos: false
    }
  },
  methods: {
    addMoreProducts(val) {
      this.dataProductos += val.productos
      this.$store.dispatch('nextPage',val)
    },
    changeStateFiltrosAplicados() {
      this.filtrosAplicados = false
    },
    setEfectos() {
      this.efectos = true
      setTimeout(() => {
        this.efectos = false
      },500);
    }
  },
  watch: {
    productos(val) {
      // console.log('cambio productos')
      this.dataProductos = val
      this.filtrosAplicados = true
      this.setEfectos()
    }
  }
}

const Magia = {
  components: {
    'v-filtros': vFiltros,
    'v-productos': vProductos
  },
  mounted() {
    this.getFirstLoad()
  },
  template: `
    <div class="row">
      <v-filtros :filtros="filtros"
        @listenLoading="setLoading($event)"
        @listenProductFiltrados="setProductFiltrados($event)"
      />
      <v-productos :productos="productos" />
      <div class="v-loading" v-if="loading">
        <div class="v-loading__spinner"></div>
      </div>
    </div>
  `,
  data() {
    return {
      filtros: '',
      productos: '',
      loading: false,
      url: window.location.pathname + window.location.search
    }
  },
  methods: {
    setProductFiltrados(val) {
      this.productos = val.productos
      this.filtros = val.filtros
    },
    setLoading(val) {
      this.loading = val
    },
    setFirstPage() {
      console.log('se ejecuto setFirstPage')
      this.$store.commit('SET_PAGES', {
        url: this.url,
        filtros: `<div>${ $(".search-single-navigator").html() }</div>`,
        productos: $('.vitrine .prateleira').html(),
      })
    },
    getFirstLoad() {
      if(!!sessionStorage.getItem('urlDestino')){
        const urlDestino = sessionStorage.getItem('urlDestino')
        const isPageProduct = urlDestino.substr(-2) === '/p'
        console.log("urlDestino",urlDestino)
        console.log("isPageProduct",isPageProduct) 

        const urlOrigen = sessionStorage.getItem('urlOrigen')
        const urlPathRouter = encodeURI(decodeURI(this.$route.fullPath))
        console.log("urlOrigen",urlOrigen)
        console.log("urlPathRouter",urlPathRouter)

        if (isPageProduct && (urlOrigen === urlPathRouter)) {
          console.log('hay que usar el storage viejo')
          this.$store.commit('SET_SESSION_STATE')
        }
        else {
          console.log('hay que usar un nuevo storage')
          this.setFirstPage()
        }
      }
      else{
        console.log('hay que cargar la primer pagina')
        this.setFirstPage()
      }
      
      const page = this.$store.getters.findPage(this.url)
      console.log('page',page)
      this.filtros = ( page === undefined ) ? `<div>${ $(".search-single-navigator").html() }</div>` : page.filtros
      this.productos = ( page === undefined ) ? $('.vitrine .prateleira').html() : page.productos

    }
  }
}

let initGoToTopPage = false;
function goToTopPage(){
  if(initGoToTopPage){
    // Realizar el scroll
    let posicion = $("#v-showcase").offset().top - 110;
    $("html, body").animate({ scrollTop: posicion}, 500);
    // Deshabilitar funcion
    initGoToTopPage = false;
  }
}