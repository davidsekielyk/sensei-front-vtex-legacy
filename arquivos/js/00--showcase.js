(()=>{

	descuento()
  redDesc()
  cuotasX()
  openFiltros()

})()


function descuento() {
	let percentage = $(".vitrina__descuento-numero");
  	percentage.each(function () {
    let value = parseInt($(this).text());
    if (value > 0) {
      $(this).addClass('show');
    }
  });
}

function redDesc(){
  let redDescuento = $(".vitrina__descuento-numero.show");
  redDescuento.each(function () {
    let value = parseInt($(this).text());
    if (value > 0) {
      $(this).show();
      $(this).text(value.toFixed() + '%');
    }
  });
}

function cuotasX() {
  $('.vitrina__installment').html(function () {
    return $(this).html().replace(/cuotas de/g,'<span class="small">X</span>').replace(/o/g,'');
        });
}

function openFiltros() {
  $("body").on("click", ".openFiltro", function () {
		$(".filtros__showcase, .openFiltro").addClass("show"),$(".ordenar__showcase.show, .openOrdenar.show").removeClass("show")
	})

  $("body").on("click", ".openFiltro.show", function () {
		$(".filtros__showcase.show, .openFiltro.show").removeClass("show")
	})

  $("body").on("click", ".openOrdenar", function () {
		$(".ordenar__showcase, .openOrdenar").addClass("show"),$(".filtros__showcase.show, .openFiltros.show").removeClass("show")
	})

  $("body").on("click", ".openOrdenar.show", function () {
		$(".ordenar__showcase.show, .openOrdenar.show").removeClass("show")
	})
}