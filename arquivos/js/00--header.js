let orderFormInnew;

$(document).ready(()=> {
	getOrderFormInnew()
	.then( orderForm => {
		orderFormInnew = orderForm
		setQtCart()
	})
})

function getOrderFormInnew() {
	return new Promise( (res, rej) => {
		vtexjs.checkout.getOrderForm()
		.then( orderForm => {
			res(orderForm)
		})
	})
}

function setQtCart(){

	let cantSkus = 0
	orderFormInnew.items.forEach(item => {
		cantSkus += item.quantity
	});

	console.log('cantidad total:',cantSkus)
	$(".shopcar__cant").html(cantSkus)

}

(() => {
    navScroll()
    navMenu()
	  getDataNav()
    cuotasX()
})()

setTimeout(() => {
	getListaBuscador();
  }, 2000)

// navbar se oculta al bajar - se ve al subir
function navScroll(){
    const body = document.body;
    const scrollUp = "scroll-up";
    const scrollDown = "scroll-down";
    let lastScroll = 0;
  
    window.addEventListener("scroll", () => {
      const currentScroll = window.pageYOffset;
      if (currentScroll <= 90) {
        body.classList.remove(scrollUp);
        return;
      }
      
      if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
        // down
        body.classList.remove(scrollUp);
        body.classList.add(scrollDown);
      } else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
        // up
        body.classList.remove(scrollDown);
        body.classList.add(scrollUp);
      }
      lastScroll = currentScroll;
    })
  }

function navMenu() {

    $("body").on("click", ".header_button-menu", function () {
        $(".header__menu,.header_button-menu,.bg__menuabierto").addClass("show")
    })

    $("body").on("click", ".header_button-menu.show,.bg__menuabierto.show", function () {
        $(".header__menu.show,.header_button-menu.show,.bg__menuabierto.show,.submenu__categorias").removeClass("show")
    })

    $("body").on("click", ".header_button-menu", function () {
        $("body").addClass("noScroll")
    })

    $("body").on("click", ".header_button-menu.show,.bg__menuabierto.show", function () {
        $("body").removeClass("noScroll")
    })

    $("body").on("click", ".menu__categorias li a", function () {
        $(this).next(".submenu__categorias").addClass("show");
    })

    $("body").on("click", ".subcat__titulo-desk", function () {
        $(".submenu__categorias.show").removeClass("show")
    })
    
}

function getListaBuscador() {
	const target = document.querySelector('.ui-autocomplete.ui-menu.ui-widget ')
	// create an observer instance
	const observer = new MutationObserver(function (mutations) {
		editListaBuscador()
	});
	// configuration of the observer:
	const config = { attributes: false, childList: true, characterData: false };
	// pass in the target node, as well as the observer options
	observer.observe(target, config);
}

function editListaBuscador(){
	// console.log("termino de cargar la lista de opciones")
	const listProduct = $(".ui-autocomplete.ui-menu.ui-widget li")
	for(let i = 0; i < listProduct.length; i++){
			//console.log(listProduct[i])
			const item = listProduct.eq(i)
			// console.log(item)
			if(!item.find("img").length){
					// console.log("no tiene imagen")
					item.addClass("searchCategory")
			}else{
					// console.log("si tiene imagen")
					const src = item.find("img").attr("src")
					// console.log("src viejo",src)
					const nuevoSrc = src.replace("-25-25","-100-100")
					// console.log("src nuevo", nuevoSrc)
					item.find("img").attr("src",nuevoSrc)
			}
	}
} 

function getDataNav(){
	$.ajax({
		url: "/api/catalog_system/pub/facets/search/a?map=c",
		success: function(data){
			console.log(data.CategoriesTrees)
			createNav(data.CategoriesTrees)
		}
	})
}

function createNav(CategoriesTrees){
	CategoriesTrees.forEach(depto => {
		// console.log('depto',depto)

		depto.Children.forEach(categoria => {
			// console.log('categoria',categoria)

			$(`.submenu__categorias.id-${depto.Id}`).append(`

			<ul class="menu_tercerNivel id-${categoria.Id}">
			<li class="titulo_menuTercerNivel"><a href="${categoria.Link}">${categoria.Name}</a></li>
			</ul>
			
		`)

			categoria.Children.forEach(subcategoria => {
				// console.log('subcategoria', subcategoria)
				$(`.menu_tercerNivel.id-${categoria.Id}`).append(`
				
					<li>
							<a href="${subcategoria.Link}">${subcategoria.Name}</a>
					</li>
				`)
			})
			
		});

	});
}

function cuotasX() {
  $('.vitrina__installment').html(function () {
    return $(this).html().replace(/cuotas de/g,'<span class="small">X</span>').replace(/o/g,'');
        });
}