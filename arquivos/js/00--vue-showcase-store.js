const store = new Vuex.Store({
  state: {
    pages: [],
    acum: []
  },
  getters: {
    findPage: ( state ) => ( dataUrl ) => {
      let url = window.location.origin + dataUrl
      url = new URL(url)
      const path = url.pathname
      const order = url.searchParams.get('O')
      return state.pages.find( page => page.url.path === path && page.url.order === order )
    }
  },
  mutations: {
    SET_PAGES ( state, data ) {

      let url = window.location.origin + data.url
      url = new URL(url)

      const acum = [...state.acum]

      state.pages.push({
        url: {
          href: data.url,
          path: url.pathname,
          order: url.searchParams.get('O')
        },
        filtros: data.filtros,
        productos: data.productos,
        acum: acum,
        pagina: 1,
      })
      
    },
    SET_ACUM ( state, data ) {
      state.acum.push(data)
    },
    REMOVE_ACUM ( state, data ) {
      const index = state.acum.indexOf(data)
      state.acum.splice(index,1)
    },
    REPLACE_ALL_ACUM ( state, data ) {
      state.acum = data
    },
    SET_SESSION_STATE ( state ) {
      const session = JSON.parse(sessionStorage.getItem('storage'))
      state.pages = session.pages
      state.acum = session.acum 
    },
    ADD_PRODUCTS ( state, data ) {
      console.log(data)
      state.pages[data.index].productos += data.productos
      state.pages[data.index].pagina += 1
    }
  },
  actions: {
    fetchPage(context, url) {
      return new Promise((res, rej) => {
        $.ajax({
          url: url,
          success: page => {
            const filtros = `<div>${ $(page).find(".search-single-navigator").html() }</div>`
            const productos = $(page).find(".vitrine .prateleira").html()
            
            context.commit('SET_PAGES', {
              url: url,
              filtros: filtros,
              productos: productos,
            })

            res({
              filtros: filtros,
              productos: productos,
            })

          }
        })
      })
    },
    nextPage(context, data) {
      console.log('context',context)
      console.log('data',data)

      const pageFound = context.getters.findPage(data.url)
      console.log('pageFound', pageFound)

      const index = context.state.pages.findIndex(page => page === pageFound)
      console.log('index',index)

      context.commit('ADD_PRODUCTS',{
        index: index,
        productos: data.productos
      })
    }
  }
})